const express = require("express");
const cors = require("cors");
var admin = require("firebase-admin");
var serviceAccount = require("./appleShop.json");
//
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "http://appleshop-angular.firebaseapp.com",
});
const db = admin.firestore();
const applesCollection = db.collection("Products");
//

var corsOptions = {
  origin: "http://localhost:4200",
  optionsSuccessStatus: 200,
};
const app = express();
app.use(cors(corsOptions));
app.use(express.json());

app.listen(8000, () => {
  console.log("Server started!");
});
// api
app.route("/api/nodeserver").get(async function (req, res) {
  // lay het du lieu colle
  const snapshots = await applesCollection.get();
  const products = [];
  snapshots.forEach((doc) => {
    products.push({
      id: doc.id,
      ...doc.data(),
    });
  });
  // products.sort((product, nextProduct) => product.id - nextProduct.id);
  console.log("Get products complete");
  res.json(products);
});

app.route("/api/nodeserver/insert").post(async function (req, res) {
  let data = { ...req.body };
  await applesCollection.add(data)
  res.json({ data });
});

app.route('/api/nodeserver/update').post(async function (req, res){
  const data = req.body;
  await applesCollection.doc(data.id).update(data)
  res.status(200);
})

app.route("/api/delete").delete(async (req, res) => {
  const {id} = req.query
  await applesCollection.doc(id).delete()
  res.status(200).send({msg: `deleted`})
});
